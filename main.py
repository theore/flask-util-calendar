import json
import logging

import flask
from apiclient.discovery import build
from flask import Flask, jsonify
import uuid
from oauth2client.contrib.flask_util import UserOAuth2

app = Flask(__name__)

# app.config['SECRET_KEY'] = 'your-secret-key'
app.config['GOOGLE_OAUTH2_CLIENT_SECRETS_FILE'] = 'client_secrets.json'

# # or, specify the client id and secret separately
# app.config['GOOGLE_OAUTH2_CLIENT_ID'] = 'your-client-id'
# app.config['GOOGLE_OAUTH2_CLIENT_SECRET'] = 'your-client-secret'

oauth2 = UserOAuth2(app)
# oauth2 = UserOAuth2(app, include_granted_scopes=True) # include_granted_scopes is already not support.
app.secret_key = str(uuid.uuid4())

@app.route('/')
def hello():
    return 'Hello World!'


# Note that app.route should be the outermost decorator.
@app.route('/needs_credentials')
@oauth2.required
def example():
    # http is authorized with the user's credentials and can be used
    # to make http calls.
    http = oauth2.http()

    # Or, you can access the credentials directly
    credentials = oauth2.credentials


@app.route('/info')
@oauth2.required
def info():
    return "Hello, {} ({})".format(oauth2.email, oauth2.user_id)


@app.route('/login')
def login():
    return oauth2.authorize_url("/")


@app.route('/calendar')
# @oauth2.required()
@oauth2.required(scopes=['https://www.googleapis.com/auth/calendar'])
def requires_calendar():

    # http = oauth2.http()
    if oauth2.has_credentials():
        logging.info("has credentilas")
        service = build('calendar', 'v3', oauth2.http())
        calendars = service.calendarList().list().execute()
        logging.info("#### ")
        logging.info(json.dumps(calendars))
        return jsonify(calendars)

    else:
        return 'No credentials!'


@app.errorhandler(500)
def server_error(e):
    # Log the error and stacktrace.
    logging.exception('An error occurred during a request.')
    return 'An internal error occurred.', 500
